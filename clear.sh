#!/bin/bash
find . -name "Icon?" -print0 | xargs -0 rm
find . -name "__pycache__" -print0 | xargs -0 rm -r

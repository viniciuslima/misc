;; INSIDE ==> (defun dotspacemacs/layers ()
  dotspacemacs-additional-packages '(jedi)
  
  dotspacemacs-configuration-layers '(themes-megapack
  
;; INSIDE ==> (defun dotspacemacs/init ()
     dotspacemacs-themes '(jbeans

;; INSIDE ==> (defun dotspacemacs/user-config ()

  ;; Setting default indentation
  (setq-default indent-tabs-mode nil)
  (setq-default tab-width 4)
  (setq indent-line-function 'insert-tab)

  ;; Configuring vertical indicator to 80 columns
  (add-hook 'after-change-major-mode-hook 'fci-mode)
  (add-hook 'after-change-major-mode-hook
    (lambda()
      (setq fci-rule-column 80)
      (setq fci-rule-use-dashes nil)
      ))

  ;; Setting line numbers
  (global-linum-mode t)
  (setq linum-format "%3s ")

  ;; Setting background to black
  (set-background-color "black")

  ;; Configuring Whitespace
  (global-set-key (kbd "C-c w") 'whitespace-mode)
  (global-set-key (kbd "C-c C-w") 'whitespace-toggle-options)
  (add-hook 'python-mode-hook
    (lambda()
      (setq whitespace-line-column '120)
      ))

  ;; Jedi autocomplete
  (global-auto-complete-mode t)
  (add-hook 'python-mode-hook 'jedi:setup)
  (setq jedi:complete-on-dot t)

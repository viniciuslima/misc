# -- ON URLS.PY FOR MEDIA SERVING ON DEBUG

from django.conf import settings
if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# -- TEMPLATES

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.static',
                'django.template.context_processors.media',
            ],
        },
    },
]


# -- LANGUAGE

LANGUAGE_CODE = 'pt-br'
TIME_ZONE = 'America/Sao_Paulo'


# -- STATIC FILES

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')


# -- MAILING

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587 #TLS
#EMAIL_PORT = 465 #SSL
EMAIL_HOST_USER = 'user@gmail.com'
EMAIL_HOST_PASSWORD = 'password'
EMAIL_USE_TLS = True
#EMAIL_USE_SSL = True
#EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


# -- LOGGING

LOG_DIR = os.path.join(BASE_DIR, 'logs/')
if not os.path.exists(LOG_DIR):
    os.makedirs(LOG_DIR)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%Y-%m-%d %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'logfile': {
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': os.path.join(LOG_DIR, 'django.log'),
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['logfile'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django': {
            'handlers': ['logfile'],
            'level': 'ERROR',
            'propagate': False,
        },
        'app': {
            'handlers': ['logfile'],
            'level': 'INFO', # Or maybe INFO or DEBUG
            'propagate': True
        },
    },
}


# -- SUIT SETTINGS

SUIT_CONFIG = {
    'ADMIN_NAME': 'Site name',
    'MENU': (
        {'app': 'APP_NAME', 'label': 'APP LABEL'},
        {'label': 'MISC DIR', 'icon':'icon-briefcase', 'models': [
            {'label': 'MISC ITEM', 'url': 'MISC ITEM URL'},
        ]},
        'auth',
    ),
}


# -- LOCAL SETTINGS

try:
    from SITE.local_settings import *
except:
    pass

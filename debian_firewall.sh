#!/bin/bash

INIT="#!/bin/sh
### BEGIN INIT INFO
# Provides:          fwinit
# Required-Start:
# Required-Stop:
# Default-Start:     2
# Default-Stop:      0
# Short-Description: Firewall script
# Description:       Start iptables-based firewall
### END INIT INFO
#
# A very basic IPtables / Netfilter script

PATH='/sbin'

# Flush the tables to apply changes
iptables -F

# Default policy to drop 'everything' but our output to internet
iptables -P FORWARD DROP
iptables -P INPUT   DROP
iptables -P OUTPUT  ACCEPT

# Allow established connections (the responses to our outgoing traffic)
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# Allow local programs that use loopback (Unix sockets)
iptables -A INPUT -s 127.0.0.0/8 -d 127.0.0.0/8 -i lo -j ACCEPT

# Allow local subnet network
#iptables -A INPUT -s 192.168.128.0/17 -j ACCEPT

# Allow SSH connections
iptables -A INPUT -p tcp --dport 6622 -m state --state NEW -j ACCEPT
# Allow HTTP connections
iptables -A INPUT -p tcp --dport 80 -m state --state NEW -j ACCEPT
"

echo "$INIT" > /etc/init.d/fwinit

chmod a+x /etc/init.d/fwinit

if [ -e /etc/rcS.d/S01fwinit ]
then
  rm /etc/rcS.d/S01fwinit;
fi

ln -s /etc/init.d/fwinit /etc/rcS.d/S01fwinit;
